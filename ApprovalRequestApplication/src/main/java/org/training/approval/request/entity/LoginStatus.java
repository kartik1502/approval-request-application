package org.training.approval.request.entity;

public enum LoginStatus {
	LOGGED_IN, LOGGED_OUT
}
