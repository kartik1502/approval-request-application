package org.training.approval.request.kafka.configuration;

import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.training.approval.request.dto.AlertApprovalDto;
import org.training.approval.request.kafka.deserializer.AlertApprovalDtoDeserializer;

@Configuration
public class AlertRequestConsumerConfiguration {

	@Bean
	ConsumerFactory<String, AlertApprovalDto> alertApprovalConsumerFactory() {

		Map<String, Object> properties = new HashMap<>();
		properties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
		properties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
		properties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, AlertApprovalDtoDeserializer.class);
		properties.put(ConsumerConfig.GROUP_ID_CONFIG, "alert-approval");
		properties.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, true);
		properties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");

		return new DefaultKafkaConsumerFactory<>(properties, new StringDeserializer(),
				new AlertApprovalDtoDeserializer());
	}

	@Bean
	ConcurrentKafkaListenerContainerFactory<String, AlertApprovalDto> concurrentKafkaListenerContainerFactory() {
		ConcurrentKafkaListenerContainerFactory<String, AlertApprovalDto> containerFactory = new ConcurrentKafkaListenerContainerFactory<>();
		containerFactory.setConsumerFactory(alertApprovalConsumerFactory());
		return containerFactory;
	}

}
