package org.training.approval.request.service;

import org.training.approval.request.dto.ActionDto;
import org.training.approval.request.dto.ResponseDto;

import jakarta.validation.Valid;

public interface ActionService {

	ResponseDto takeAction(String assetRequestId, @Valid ActionDto actionDto);

}
