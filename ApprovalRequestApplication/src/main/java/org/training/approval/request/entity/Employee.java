package org.training.approval.request.entity;

import java.time.LocalDateTime;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Employee {

	@Id
	private String employeeId;
	
	private String firstName;
	
	private String lastName;
	
	private String emailId;
	
	private String contactNumber;
	
	@ManyToOne
	@JoinColumn(name = "managerId")
	private Employee managerId;
	
	@Enumerated(EnumType.STRING)
	private LoginStatus loginStatus;
	
	@CreationTimestamp
	private LocalDateTime creationTime;
	
	@UpdateTimestamp
	private LocalDateTime updationTime;
}
