package org.training.approval.request.service;

import org.training.approval.request.dto.RaiseRequestDto;
import org.training.approval.request.dto.ResponseDto;

public interface RaiseRequestService {

	ResponseDto raiseAssetRequest(RaiseRequestDto raiseRequestDto);

}
