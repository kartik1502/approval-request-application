package org.training.approval.request.dto;

import java.time.LocalDateTime;

public record ApprovalsDto(String requestId, String sourceApplicationId, String sourceRequestId, String requesterName,
		LocalDateTime lastUpdateTime, String reason, String requestStatus) {

}
