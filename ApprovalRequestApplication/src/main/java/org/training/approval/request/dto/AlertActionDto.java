package org.training.approval.request.dto;

public record AlertActionDto(String sourceRequestId, String sourceApplicationId, String requestStatus) {

}
