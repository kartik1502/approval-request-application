package org.training.approval.request.service;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.training.approval.request.dto.AlertApprovalDto;

public interface MessageReader {

	void alertRequestApproval(ConsumerRecord<String, AlertApprovalDto> message);
}
