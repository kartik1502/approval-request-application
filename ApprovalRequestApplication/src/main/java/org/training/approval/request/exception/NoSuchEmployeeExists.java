package org.training.approval.request.exception;

import org.training.approval.request.constants.GlobalConstants;

public class NoSuchEmployeeExists extends GlobalException {

	private static final long serialVersionUID = 1L;

	public NoSuchEmployeeExists(String errorMessage) {
		super(GlobalConstants.ERROR_NOT_FOUND, GlobalConstants.NOT_FOUND, errorMessage);
	}

}
