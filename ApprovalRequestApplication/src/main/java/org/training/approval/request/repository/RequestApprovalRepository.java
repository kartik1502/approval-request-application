package org.training.approval.request.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.training.approval.request.entity.Employee;
import org.training.approval.request.entity.RequestApproval;
import org.training.approval.request.entity.RequestStatus;

public interface RequestApprovalRepository extends JpaRepository<RequestApproval, String> {

	Optional<RequestApproval> findBySourceRequestId(String sourceRequestId);
	
	List<RequestApproval> findByApproverIdAndRequestStatus(Employee approverId, RequestStatus requestStatus);
	
	List<RequestApproval> findByRequesterId(Employee requesterId);
}
