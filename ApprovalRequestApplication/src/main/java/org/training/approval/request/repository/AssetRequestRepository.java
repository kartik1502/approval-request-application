package org.training.approval.request.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.training.approval.request.entity.AssetRequest;

public interface AssetRequestRepository extends JpaRepository<AssetRequest, String> {

	List<AssetRequest> findByAssetRequestIdIn(List<String> requestIds);
}
