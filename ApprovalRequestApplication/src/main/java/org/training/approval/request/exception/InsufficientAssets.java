package org.training.approval.request.exception;

import org.training.approval.request.constants.GlobalConstants;

public class InsufficientAssets extends GlobalException {

	private static final long serialVersionUID = 1L;

	public InsufficientAssets(String errorMessage) {
		super(GlobalConstants.ERROR_INSUFFICIENT_ASSETS, GlobalConstants.BAD_REQUEST, errorMessage);
	}

}
