package org.training.approval.request.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.training.approval.request.dto.ActionDto;
import org.training.approval.request.dto.ResponseDto;
import org.training.approval.request.service.ActionService;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
public class ActionController {

	private final ActionService actionService;
	
	@PutMapping("/asset-requests/{assetRequestId}")
	public ResponseEntity<ResponseDto> takeAction(@PathVariable String assetRequestId, @RequestBody @Valid ActionDto actionDto) {
		return ResponseEntity.ok(actionService.takeAction(assetRequestId, actionDto));
	}
}
