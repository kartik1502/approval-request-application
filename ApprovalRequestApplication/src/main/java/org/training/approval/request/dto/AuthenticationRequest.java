package org.training.approval.request.dto;

public record AuthenticationRequest(String emailId, String password) {

}
