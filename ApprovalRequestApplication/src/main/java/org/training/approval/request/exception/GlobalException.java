package org.training.approval.request.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class GlobalException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	private String errorCode;
	
	private String statusCode;
	
	private String errorMessage;
}
