package org.training.approval.request.dto;

import org.training.approval.request.constants.GlobalConstants;

import jakarta.validation.constraints.Size;

public record RaiseRequestDto(
		String assetId,
		String requesterId,
		String approverId,
		@Size(max = 250, message = GlobalConstants.RESTRICT_LENGTH) String reason) {

}
