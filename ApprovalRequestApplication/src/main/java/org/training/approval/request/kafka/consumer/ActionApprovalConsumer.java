package org.training.approval.request.kafka.consumer;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import org.training.approval.request.constants.GlobalConstants;
import org.training.approval.request.dto.AlertActionDto;
import org.training.approval.request.service.RequestApprovalService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class ActionApprovalConsumer {

	private final RequestApprovalService requestApprovalService;

	@KafkaListener(topics = GlobalConstants.KAFKA_ALERT_ACTION_REQUEST_APPROVAL_TOPIC, groupId = "alert-action-approval", containerFactory = "actionApprovalKafkaListenerContainerFactory")
	public void comsumeActionApproval(ConsumerRecord<String, AlertActionDto> consumerRecord) {

		log.info("Message: {} received from topic:{} with key:{}", consumerRecord.value(), consumerRecord.topic(),
				consumerRecord.key());
		requestApprovalService.updateActionRequestApproval(consumerRecord.value());
	}
}
