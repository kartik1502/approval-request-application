package org.training.approval.request.service.implementation;

import java.math.BigDecimal;

import org.springframework.stereotype.Service;
import org.training.approval.request.constants.GlobalConstants;
import org.training.approval.request.dto.AlertApprovalDto;
import org.training.approval.request.dto.RaiseRequestDto;
import org.training.approval.request.dto.ResponseDto;
import org.training.approval.request.entity.Asset;
import org.training.approval.request.entity.AssetRequest;
import org.training.approval.request.entity.Employee;
import org.training.approval.request.entity.LoginStatus;
import org.training.approval.request.entity.RequestLog;
import org.training.approval.request.entity.RequestStatus;
import org.training.approval.request.exception.InsufficientAssets;
import org.training.approval.request.exception.LoginRequiredException;
import org.training.approval.request.exception.NoSuchAssetFound;
import org.training.approval.request.exception.NoSuchEmployeeExists;
import org.training.approval.request.kafka.producer.AlertRequestProducer;
import org.training.approval.request.repository.AssetRepository;
import org.training.approval.request.repository.AssetRequestRepository;
import org.training.approval.request.repository.EmployeeRepository;
import org.training.approval.request.repository.RequestLogRepository;
import org.training.approval.request.service.RaiseRequestService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class RaiseRequestServiceImpl implements RaiseRequestService {

	private final EmployeeRepository employeeRepository;
	private final AssetRepository assetRepository;
	private final AssetRequestRepository assetRequestRepository;
	private final RequestLogRepository requestLogRepository;
	private final AlertRequestProducer alertRequestProducer;

	@Override
	public ResponseDto raiseAssetRequest(RaiseRequestDto raiseRequestDto) {

		Employee requestor = employeeRepository.findById(raiseRequestDto.requesterId()).orElseThrow(() -> {
			log.error(GlobalConstants.LOG_EMPLOYEE_NOT_FOUND);
			throw new NoSuchEmployeeExists(GlobalConstants.REQUESTER_NOT_FOUND);
		});

		if (requestor.getLoginStatus().equals(LoginStatus.LOGGED_OUT)) {
			log.error(GlobalConstants.LOG_UN_AUTHORIZED);
			throw new LoginRequiredException(GlobalConstants.LOGIN_REQUIRED);
		}

		Asset asset = assetRepository.findById(raiseRequestDto.assetId()).orElseThrow(() -> {
			log.error(GlobalConstants.LOG_ASSET_NOT_FOUND);
			throw new NoSuchAssetFound(GlobalConstants.ASSET_NOT_FOUND);
		});

		if (asset.getQuantity().compareTo(BigDecimal.ZERO) <= 0) {
			log.error(GlobalConstants.LOG_INSUFFICIENT_ASSETS);
			throw new InsufficientAssets(GlobalConstants.INSUFFICIENT_ASSETS);
		}
		asset.setQuantity(asset.getQuantity().subtract(BigDecimal.valueOf(1)));

		Employee approver = employeeRepository.findById(raiseRequestDto.approverId()).orElseThrow(() -> {
			log.error(GlobalConstants.LOG_EMPLOYEE_NOT_FOUND);
			throw new NoSuchEmployeeExists(GlobalConstants.APPROVER_NOT_FOUND);
		});

		AssetRequest assetRequest = AssetRequest.builder()
				.assetRequestId(String.format("%s%04d", GlobalConstants.ASSET_REQUEST_PREFIX,
						assetRequestRepository.count() + 1))
				.requesterId(requestor).approverId(approver).asset(asset).reason(raiseRequestDto.reason())
				.requestStatus(RequestStatus.SUBMITTED).build();

		RequestLog requestLog = RequestLog.builder()
				.requestLogId(
						String.format("%s%04d", GlobalConstants.REQUEST_LOG_PREFIX, requestLogRepository.count() + 1))
				.action(RequestStatus.SUBMITTED).actionBy(requestor).remarks(raiseRequestDto.reason())
				.requestId(assetRequest.getAssetRequestId()).build();

		alertRequestProducer.alertRequestApproval(new AlertApprovalDto(GlobalConstants.ASSET_REQUEST_APPLICATION,
				assetRequest.getAssetRequestId(), assetRequest.getRequesterId().getEmployeeId(),
				assetRequest.getApproverId().getEmployeeId(), RequestStatus.PENDING.name())); 

		assetRepository.save(asset);
		assetRequestRepository.save(assetRequest);
		requestLogRepository.save(requestLog);
		return new ResponseDto(GlobalConstants.SUCCESS_OK, GlobalConstants.ASSET_REQUEST_SUCCESS);
	}

}
