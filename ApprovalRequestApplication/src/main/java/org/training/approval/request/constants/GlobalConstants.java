package org.training.approval.request.constants;

public interface GlobalConstants {
	
	//kafka constants
	String KAFKA_ALERT_NEW_REQUEST_APPROVAL_TOPIC = "alert-new-request-approval";
	String KAFKA_ALERT_ACTION_REQUEST_APPROVAL_TOPIC = "alert-action-request-approval";
	String NULL_VALUE_SERIALIZER = "null value received at serializer";
	String NULL_VALUE_DESERIALIZER = "null value received at deserializer";
	String SERIALIZER_ERROR = "error occurred while serializer";
	String DESERIALIZER_ERROR = "error occurred while deserializer";
	String KAFKA_ASSET_REQUEST_KEY = "ASREQ";
	String KAFKA_ACTION_KEY = "ASREQACT";
	String KAFKA_PRODUCER_ERROR = "error occurred while sending message to kafka";
	
	//generator prefix's
	String ASSET_REQUEST_PREFIX = "ASREQ";
	String REQUEST_LOG_PREFIX = "REQLG";
	String REQUEST_APPROVAL_PREFIX = "REQAPP";
	String ASSET_REQUEST_APPLICATION = "ASREQ";

	//HTTP error status codes
	String BAD_REQUEST = "400";
	String NOT_FOUND = "404";
	String UN_AUTHORIZED = "401";
	String FORBIDDEN = "403";
	String CONFLICT = "409";
	
	//status codes
	String ERROR_NOT_FOUND = "ERRNF_404";
	String ERROR_UN_AUTHORIZED = "ERRUA_401";
	String SUCCESS_OK = "SUCCESS_200";
	String SUCCESS_CREATED = "SUCCESS_201";
	String ERROR_INSUFFICIENT_ASSETS = "ERRIA_400";
	String ERROR_LOGIN_REQUIRED = "ERRLR_401";
	String ERROR_LOGIN_CONFLICT ="ERRLC_403";
	String ERROR_REQUEST_FORBIDDEN = "ERREQFB_403";
	String ERROR_CONFLICT = "ERRCL_409";
	
	//log messages
	String LOG_EMPLOYEE_NOT_FOUND = "employee not found";
	String LOG_INVALID_CREDENTIALS = "invalid credentials";
	String LOG_ASSET_NOT_FOUND = "asset not found";
	String LOG_INSUFFICIENT_ASSETS = "insufficient assets";
	String LOG_UN_AUTHORIZED = "un authorized access";
	String LOG_LOGIN_CONFLICT = "logged in already";
	String LOG_REQUEST_NOT_FOUND = "request not found";
	String LOG_REQUEST_FORBIDDEN = "request is forbidden";
	String LOG_ACTION_CONFLICT = "action already taken";
	
	//error messages
	String EMPLOYEE_NOT_FOUND = "Not Found: Requested Employee does not exists";
	String REQUESTER_NOT_FOUND = "Not Found: Employee raising a request does not exists";
	String APPROVER_NOT_FOUND = "Not Found: Approver assigned for the request not found";
	String INVALID_CREDENTIALS = "Un Authorized: Plese verify the password";
	String RESTRICT_LENGTH = "Max Length: Please restrict the length to 250 characters";
	String ASSET_NOT_FOUND = "Not Found: Requested asset not found";
	String INSUFFICIENT_ASSETS = "Insufficient: Requested Asset quantity is not available";
	String LOGIN_REQUIRED = "Un Authorized: You need to login to access";
	String LOGIN_CONFLICT = "Forbidden: You have already logged in on other browser";
	String REQUEST_STATUS_VALIDATION = "Invalid status: Request status must be APPROVED or REJECTED or REFER_BACK";
	String ASSET_REQUEST_NOT_FOUND = "Not Found: Requested asset not found";
	String ASSET_REQUEST_FORBIDDEN = "Forbidden: You are not authorized to access the request";
	String ACTION_CONFLICT = "Conflict: Action already taken on the request";
	
	//success messages
	String LOGIN_SUCCESS = "Logged In: Employee successfully logged In";
	String ASSET_REQUEST_SUCCESS = "Request Raised: Asset request raised successfully";
	String ACTION_SUCCESS = "Action Taken: Action on the request is taken";
}
