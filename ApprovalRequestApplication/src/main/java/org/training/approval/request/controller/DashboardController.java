package org.training.approval.request.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.training.approval.request.dto.DashboardDto;
import org.training.approval.request.service.DashboardService;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
public class DashboardController {
	
	private final DashboardService dashboardService;

	@GetMapping("/request-approvals/{employeeId}")
	public ResponseEntity<DashboardDto> viewDashboard(@PathVariable String employeeId) {
		return ResponseEntity.ok(dashboardService.viewDashboard(employeeId));
	}
}
