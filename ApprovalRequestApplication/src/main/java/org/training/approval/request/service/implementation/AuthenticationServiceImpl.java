package org.training.approval.request.service.implementation;

import org.springframework.stereotype.Service;
import org.training.approval.request.constants.GlobalConstants;
import org.training.approval.request.dto.AuthenticationRequest;
import org.training.approval.request.dto.ResponseDto;
import org.training.approval.request.entity.Employee;
import org.training.approval.request.entity.LoginStatus;
import org.training.approval.request.exception.InvalidCredentials;
import org.training.approval.request.exception.LoginConflict;
import org.training.approval.request.exception.NoSuchEmployeeExists;
import org.training.approval.request.repository.EmployeeRepository;
import org.training.approval.request.service.AuthenticationService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class AuthenticationServiceImpl implements AuthenticationService {

	private final EmployeeRepository employeeRepository;

	@Override
	public ResponseDto authentication(AuthenticationRequest request) {

		Employee employee = employeeRepository.findById(request.emailId()).orElseThrow(() -> {
			log.error(GlobalConstants.LOG_EMPLOYEE_NOT_FOUND);
			throw new NoSuchEmployeeExists(GlobalConstants.EMPLOYEE_NOT_FOUND);
		});
		if (employee.getLoginStatus().equals(LoginStatus.LOGGED_IN)) {
			log.error(GlobalConstants.LOG_LOGIN_CONFLICT);
			throw new LoginConflict(GlobalConstants.LOGIN_CONFLICT);
		}
		if (!employee.getEmailId().equals(request.password())) {
			log.error(GlobalConstants.LOG_INVALID_CREDENTIALS);
			throw new InvalidCredentials(GlobalConstants.INVALID_CREDENTIALS);
		}
		employee.setLoginStatus(LoginStatus.LOGGED_IN);
		employeeRepository.save(employee);
		return new ResponseDto(GlobalConstants.SUCCESS_OK, GlobalConstants.LOGIN_SUCCESS);
	}

}
