package org.training.approval.request.dto;

import java.time.LocalDateTime;

public record RequestsDto(String requestId, String sourceApplicationId, String sourceRequestId, 
		String approverName, LocalDateTime lastUpdateOn, String requestStatus) {

}
 