package org.training.approval.request.exception;

import org.training.approval.request.constants.GlobalConstants;

public class LoginConflict extends GlobalException {

	private static final long serialVersionUID = 1L;

	public LoginConflict(String errorMessage) {
		super(GlobalConstants.ERROR_LOGIN_CONFLICT, GlobalConstants.FORBIDDEN, errorMessage);
	}
	
	

}
