package org.training.approval.request.service;

import org.training.approval.request.dto.AuthenticationRequest;
import org.training.approval.request.dto.ResponseDto;

import jakarta.validation.Valid;

public interface AuthenticationService {

	ResponseDto authentication(@Valid AuthenticationRequest request);

}
