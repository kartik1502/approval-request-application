package org.training.approval.request.dto;

public record AlertApprovalDto(String sourceApplicationId, String sourceRequestId, String requesterId,
		String approverId, String requestStatus) {

}
