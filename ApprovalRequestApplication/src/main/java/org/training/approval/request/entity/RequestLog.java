package org.training.approval.request.entity;

import java.time.LocalDateTime;

import org.hibernate.annotations.CreationTimestamp;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RequestLog {

	@Id
	private String requestLogId;
	
	private String requestId;
	
	@Enumerated(EnumType.STRING)
	private RequestStatus action;
	
	private String remarks;
	
	@ManyToOne
	@JoinColumn(name = "action_by")
	private Employee actionBy;
	
	@CreationTimestamp
	private LocalDateTime actionOn;
}
