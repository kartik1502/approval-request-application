package org.training.approval.request.dto;

import java.util.List;

public record DashboardDto(List<RequestsDto> requests, List<ApprovalsDto> approvals) {

}
