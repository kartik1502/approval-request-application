package org.training.approval.request.dto;

import java.time.LocalDateTime;
import java.util.List;

public record AssetRequestDto(String requestId, String assetName, String requesterName, String approverName,
		LocalDateTime createdOn, LocalDateTime lastUpdateOn, String requestStatus, List<RequestLogDto> requestHistory) {

}
