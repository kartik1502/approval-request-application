package org.training.approval.request.service;

import org.training.approval.request.dto.AlertActionDto;
import org.training.approval.request.dto.AlertApprovalDto;

public interface RequestApprovalService {

	void updateRequestApproval(AlertApprovalDto alertApprovalDto);
	
	void updateActionRequestApproval(AlertActionDto alertActionDto);
}
