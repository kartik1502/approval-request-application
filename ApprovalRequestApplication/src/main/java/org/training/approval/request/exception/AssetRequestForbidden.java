package org.training.approval.request.exception;

import org.training.approval.request.constants.GlobalConstants;

public class AssetRequestForbidden extends GlobalException {

	private static final long serialVersionUID = 1L;

	public AssetRequestForbidden(String errorMessage) {
		super(GlobalConstants.ERROR_REQUEST_FORBIDDEN, GlobalConstants.FORBIDDEN, errorMessage);
	}

}
