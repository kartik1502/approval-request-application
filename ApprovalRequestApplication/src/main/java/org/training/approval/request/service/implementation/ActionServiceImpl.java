package org.training.approval.request.service.implementation;

import org.springframework.stereotype.Service;
import org.training.approval.request.constants.GlobalConstants;
import org.training.approval.request.dto.ActionDto;
import org.training.approval.request.dto.AlertActionDto;
import org.training.approval.request.dto.ResponseDto;
import org.training.approval.request.entity.AssetRequest;
import org.training.approval.request.entity.Employee;
import org.training.approval.request.entity.LoginStatus;
import org.training.approval.request.entity.RequestLog;
import org.training.approval.request.entity.RequestStatus;
import org.training.approval.request.exception.ActionConflict;
import org.training.approval.request.exception.AssetRequestForbidden;
import org.training.approval.request.exception.LoginRequiredException;
import org.training.approval.request.exception.NoSuchAssetRequest;
import org.training.approval.request.exception.NoSuchEmployeeExists;
import org.training.approval.request.kafka.producer.AssetActionProducer;
import org.training.approval.request.repository.AssetRequestRepository;
import org.training.approval.request.repository.EmployeeRepository;
import org.training.approval.request.repository.RequestLogRepository;
import org.training.approval.request.service.ActionService;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class ActionServiceImpl implements ActionService {

	private final EmployeeRepository employeeRepository;
	private final AssetRequestRepository assetRequestRepository;
	private final RequestLogRepository requestLogRepository;
	private final AssetActionProducer assetActionProducer;

	@Override
	public ResponseDto takeAction(String assetRequestId, @Valid ActionDto actionDto) {

		Employee approver = employeeRepository.findById(actionDto.approverId()).orElseThrow(() -> {
			log.error(GlobalConstants.LOG_EMPLOYEE_NOT_FOUND);
			throw new NoSuchEmployeeExists(GlobalConstants.APPROVER_NOT_FOUND);
		});
		if (approver.getLoginStatus().equals(LoginStatus.LOGGED_OUT)) {
			log.error(GlobalConstants.LOG_UN_AUTHORIZED);
			throw new LoginRequiredException(GlobalConstants.LOGIN_REQUIRED);
		}

		AssetRequest assetRequest = assetRequestRepository.findById(assetRequestId).orElseThrow(() -> {
			log.error(GlobalConstants.LOG_REQUEST_NOT_FOUND);
			throw new NoSuchAssetRequest(GlobalConstants.ASSET_REQUEST_NOT_FOUND);
		});

		if (!assetRequest.getApproverId().equals(approver)) {
			log.error(GlobalConstants.LOG_REQUEST_FORBIDDEN);
			throw new AssetRequestForbidden(GlobalConstants.ASSET_REQUEST_FORBIDDEN);
		}
		if (!(assetRequest.getRequestStatus().equals(RequestStatus.SUBMITTED)
				|| assetRequest.getRequestStatus().equals(RequestStatus.RE_SUBMITTED))) {
			log.error(GlobalConstants.LOG_ACTION_CONFLICT);
			throw new ActionConflict(GlobalConstants.ACTION_CONFLICT);
		}
		assetRequest.setRequestStatus(RequestStatus.valueOf(actionDto.requestStatus()));

		RequestLog requestLog = RequestLog.builder()
				.requestLogId(
						String.format("%s%04d", GlobalConstants.REQUEST_LOG_PREFIX, requestLogRepository.count() + 1))
				.action(assetRequest.getRequestStatus()).requestId(assetRequestId).actionBy(approver)
				.remarks(actionDto.reason()).build();

		assetActionProducer.alertAssetAction(new AlertActionDto(assetRequestId,
				GlobalConstants.ASSET_REQUEST_APPLICATION, assetRequest.getRequestStatus().name()));

		assetRequestRepository.save(assetRequest);
		requestLogRepository.save(requestLog);
		return new ResponseDto(GlobalConstants.SUCCESS_OK, GlobalConstants.ACTION_SUCCESS);
	}

}
