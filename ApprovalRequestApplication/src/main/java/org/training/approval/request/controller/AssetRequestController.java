package org.training.approval.request.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.training.approval.request.dto.AssetRequestDto;
import org.training.approval.request.service.AssetRequestService;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping("/asset-requests")
public class AssetRequestController {

	private final AssetRequestService assetRequestService;
	
	@GetMapping("/{assetRequestId}")
	public ResponseEntity<AssetRequestDto> viewRequestDetails(@PathVariable String assetRequestId, @RequestParam String employeeId) {
		return ResponseEntity.ok(assetRequestService.viewRequestDetails(assetRequestId, employeeId));
	}

}