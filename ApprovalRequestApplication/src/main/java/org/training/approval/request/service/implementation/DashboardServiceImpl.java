package org.training.approval.request.service.implementation;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;
import org.training.approval.request.constants.GlobalConstants;
import org.training.approval.request.dto.ApprovalsDto;
import org.training.approval.request.dto.DashboardDto;
import org.training.approval.request.dto.RequestsDto;
import org.training.approval.request.entity.AssetRequest;
import org.training.approval.request.entity.Employee;
import org.training.approval.request.entity.LoginStatus;
import org.training.approval.request.entity.RequestApproval;
import org.training.approval.request.entity.RequestStatus;
import org.training.approval.request.exception.LoginRequiredException;
import org.training.approval.request.exception.NoSuchEmployeeExists;
import org.training.approval.request.repository.AssetRequestRepository;
import org.training.approval.request.repository.EmployeeRepository;
import org.training.approval.request.repository.RequestApprovalRepository;
import org.training.approval.request.service.DashboardService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class DashboardServiceImpl implements DashboardService {

	private final EmployeeRepository employeeRepository;
	private final RequestApprovalRepository requestApprovalRepository;
	private final AssetRequestRepository assetRequestRepository;

	@Override
	public DashboardDto viewDashboard(String employeeId) {

		Employee employee = employeeRepository.findById(employeeId).orElseThrow(() -> {
			log.error(GlobalConstants.LOG_EMPLOYEE_NOT_FOUND);
			throw new NoSuchEmployeeExists(GlobalConstants.EMPLOYEE_NOT_FOUND);
		});
		if (employee.getLoginStatus().equals(LoginStatus.LOGGED_OUT)) {
			log.error(GlobalConstants.LOG_UN_AUTHORIZED);
			throw new LoginRequiredException(GlobalConstants.LOGIN_REQUIRED);
		}

		List<RequestsDto> requests = requestApprovalRepository.findByRequesterId(employee).stream().map(request -> {
			return new RequestsDto(request.getRequestApprovalId(), request.getSourceApplicationId(),
					request.getSourceRequestId(),
					String.format("%s %s", request.getApproverId().getFirstName(),
							request.getApproverId().getLastName()),
					request.getRequestUpdateTime(), request.getRequestStatus().name());
		}).toList();

		List<RequestApproval> requestApprovals = requestApprovalRepository.findByApproverIdAndRequestStatus(employee,
				RequestStatus.PENDING);
		List<String> requestIds = requestApprovals.stream().map(RequestApproval::getSourceRequestId).toList();

		Map<String, AssetRequest> requestMap = assetRequestRepository.findByAssetRequestIdIn(requestIds).stream()
				.collect(Collectors.toMap(AssetRequest::getAssetRequestId, Function.identity()));

		List<ApprovalsDto> approvals = requestApprovals.stream().map(request -> {
			return new ApprovalsDto(request.getRequestApprovalId(), request.getSourceApplicationId(),
					request.getSourceRequestId(),
					String.format("%s %s", request.getRequesterId().getFirstName(),
							request.getRequesterId().getLastName()),
					request.getRequestUpdateTime(), requestMap.get(request.getSourceRequestId()).getReason(),
					request.getRequestStatus().name());
		}).toList();

		return new DashboardDto(requests, approvals);
	}

}
