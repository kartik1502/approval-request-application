package org.training.approval.request.service.implementation;

import java.util.List;

import org.springframework.stereotype.Service;
import org.training.approval.request.constants.GlobalConstants;
import org.training.approval.request.dto.AssetRequestDto;
import org.training.approval.request.dto.RequestLogDto;
import org.training.approval.request.entity.AssetRequest;
import org.training.approval.request.entity.Employee;
import org.training.approval.request.entity.LoginStatus;
import org.training.approval.request.exception.LoginRequiredException;
import org.training.approval.request.exception.NoSuchAssetRequest;
import org.training.approval.request.exception.NoSuchEmployeeExists;
import org.training.approval.request.repository.AssetRequestRepository;
import org.training.approval.request.repository.EmployeeRepository;
import org.training.approval.request.repository.RequestLogRepository;
import org.training.approval.request.service.AssetRequestService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class AssetRequestServiceImpl implements AssetRequestService {

	private final EmployeeRepository employeeRepository;
	private final AssetRequestRepository assetRequestRepository;
	private final RequestLogRepository requestLogRepository;

	@Override
	public AssetRequestDto viewRequestDetails(String assetRequestId, String employeeId) {

		Employee employee = employeeRepository.findById(employeeId).orElseThrow(() -> {
			log.error(GlobalConstants.LOG_EMPLOYEE_NOT_FOUND);
			throw new NoSuchEmployeeExists(GlobalConstants.EMPLOYEE_NOT_FOUND);
		});

		if (employee.getLoginStatus().equals(LoginStatus.LOGGED_OUT)) {
			log.error(GlobalConstants.LOG_UN_AUTHORIZED);
			throw new LoginRequiredException(GlobalConstants.LOGIN_REQUIRED);
		}

		AssetRequest assetRequest = assetRequestRepository.findById(assetRequestId).orElseThrow(() -> {
			log.error(GlobalConstants.LOG_REQUEST_NOT_FOUND);
			throw new NoSuchAssetRequest(GlobalConstants.ASSET_REQUEST_NOT_FOUND);
		});

		List<RequestLogDto> requestHistory = requestLogRepository.findByRequestId(assetRequestId).stream()
				.map(requestLog -> {
					return new RequestLogDto(requestLog.getRequestLogId(), requestLog.getAction().name(),
							String.format("%s %s", requestLog.getActionBy().getFirstName(),
									requestLog.getActionBy().getLastName()),
							requestLog.getRemarks(), requestLog.getActionOn());
				}).toList();

		return new AssetRequestDto(assetRequestId, assetRequest.getAsset().getAssetName(),
				String.format("%s %s", assetRequest.getRequesterId().getFirstName(),
						assetRequest.getRequesterId().getLastName()),
				String.format("%s %s", assetRequest.getApproverId().getFirstName(),
						assetRequest.getApproverId().getLastName()),
				assetRequest.getRequestCreationTime(), assetRequest.getRequestUpdateTime(),
				assetRequest.getRequestStatus().name(), requestHistory);
	}

}
