package org.training.approval.request.exception;

import org.training.approval.request.constants.GlobalConstants;

public class ActionConflict extends GlobalException {

	private static final long serialVersionUID = 1L;

	public ActionConflict(String errorMessage) {
		super(GlobalConstants.ERROR_CONFLICT, GlobalConstants.CONFLICT, errorMessage);
	}

}
