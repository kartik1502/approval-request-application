package org.training.approval.request.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.training.approval.request.dto.AuthenticationRequest;
import org.training.approval.request.dto.ResponseDto;
import org.training.approval.request.service.AuthenticationService;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping("/auth")
public class AuthenticationController {
	
	private final AuthenticationService authenticationService;

	@PostMapping("/login")
	public ResponseEntity<ResponseDto> authenticateEmployee(@RequestBody @Valid AuthenticationRequest request) {
		return ResponseEntity.ok(authenticationService.authentication(request));
	}
}
