package org.training.approval.request.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.training.approval.request.entity.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, String> {
	
	Optional<Employee> findEmployeeByEmailId(String emailId);

}
