package org.training.approval.request.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.training.approval.request.entity.RequestLog;

public interface RequestLogRepository extends JpaRepository<RequestLog, String> {
	
	List<RequestLog> findByRequestId(String requestId);

}
