package org.training.approval.request.service.implementation;

import org.springframework.stereotype.Service;
import org.training.approval.request.constants.GlobalConstants;
import org.training.approval.request.dto.AlertActionDto;
import org.training.approval.request.dto.AlertApprovalDto;
import org.training.approval.request.entity.Employee;
import org.training.approval.request.entity.RequestApproval;
import org.training.approval.request.entity.RequestStatus;
import org.training.approval.request.exception.NoSuchAssetRequest;
import org.training.approval.request.exception.NoSuchEmployeeExists;
import org.training.approval.request.repository.EmployeeRepository;
import org.training.approval.request.repository.RequestApprovalRepository;
import org.training.approval.request.service.RequestApprovalService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class RequestApprovalServiceImpl implements RequestApprovalService {

	private final EmployeeRepository employeeRepository;
	private final RequestApprovalRepository requestApprovalRepository;

	@Override
	public void updateRequestApproval(AlertApprovalDto alertApprovalDto) {

		Employee requester = employeeRepository.findById(alertApprovalDto.requesterId()).orElseThrow(() -> {
			log.error(GlobalConstants.LOG_EMPLOYEE_NOT_FOUND);
			throw new NoSuchEmployeeExists(GlobalConstants.EMPLOYEE_NOT_FOUND);
		});

		Employee approver = employeeRepository.findById(alertApprovalDto.approverId()).orElseThrow(() -> {
			log.error(GlobalConstants.LOG_EMPLOYEE_NOT_FOUND);
			throw new NoSuchEmployeeExists(GlobalConstants.EMPLOYEE_NOT_FOUND);
		});

		RequestApproval requestApproval = RequestApproval.builder()
				.requestApprovalId(String.format("%s%04d", GlobalConstants.REQUEST_APPROVAL_PREFIX,
						requestApprovalRepository.count() + 1))
				.sourceApplicationId(GlobalConstants.ASSET_REQUEST_APPLICATION).requesterId(requester)
				.approverId(approver).sourceRequestId(alertApprovalDto.sourceRequestId())
				.requestStatus(RequestStatus.valueOf(alertApprovalDto.requestStatus())).build();
		
		requestApprovalRepository.save(requestApproval);
	}

	@Override
	public void updateActionRequestApproval(AlertActionDto alertActionDto) {
		
		RequestApproval requestApproval = requestApprovalRepository.findBySourceRequestId(alertActionDto.sourceRequestId())
				.orElseThrow(() -> {
					log.error(GlobalConstants.LOG_REQUEST_NOT_FOUND);
					throw new NoSuchAssetRequest(GlobalConstants.ASSET_REQUEST_NOT_FOUND);
				});
		
		requestApproval.setSourceApplicationId(alertActionDto.sourceApplicationId());
		requestApproval.setRequestStatus(RequestStatus.valueOf(alertActionDto.requestStatus()));
		requestApprovalRepository.save(requestApproval);
	}

}
