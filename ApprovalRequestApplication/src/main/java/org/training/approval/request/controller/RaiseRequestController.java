package org.training.approval.request.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.training.approval.request.dto.RaiseRequestDto;
import org.training.approval.request.dto.ResponseDto;
import org.training.approval.request.service.RaiseRequestService;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping("/asset-requests")
public class RaiseRequestController {
	
	private final RaiseRequestService raiseRequestService;
	
	@PostMapping
	public ResponseEntity<ResponseDto> raiseAssetRequest(@RequestBody @Valid RaiseRequestDto raiseRequestDto) {
		return ResponseEntity.status(HttpStatus.CREATED)
				.body(raiseRequestService.raiseAssetRequest(raiseRequestDto));
	}
}
