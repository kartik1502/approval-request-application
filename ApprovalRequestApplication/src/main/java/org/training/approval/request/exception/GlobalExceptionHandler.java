package org.training.approval.request.exception;

import java.util.List;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import org.training.approval.request.constants.GlobalConstants;

@RestControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

	protected ResponseEntity<Object> handleMethodArgumentNotValid(
			MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatusCode status, WebRequest request) {

		List<String> errors = ex.getBindingResult().getAllErrors().stream().map(ObjectError::getDefaultMessage).toList();
		return ResponseEntity.badRequest()
				.body(new GlobalErrorResponse(errors, GlobalConstants.BAD_REQUEST));
	}
	
	@ExceptionHandler(GlobalException.class)
	public ResponseEntity<Object> handleGlobalException(GlobalException ex) {
		
		return ResponseEntity
				.status(HttpStatus.valueOf(Integer.valueOf(ex.getStatusCode())))
				.body(ErrorResponse.builder()
						.errorCode(ex.getErrorCode())
						.errorMessage(ex.getErrorMessage()).build());
	}
}
