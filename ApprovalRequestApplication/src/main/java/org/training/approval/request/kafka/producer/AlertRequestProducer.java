package org.training.approval.request.kafka.producer;

import java.util.Objects;
import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.stereotype.Service;
import org.training.approval.request.constants.GlobalConstants;
import org.training.approval.request.dto.AlertApprovalDto;
import org.training.approval.request.kafka.serializer.AlertApproalDtoSerializer;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AlertRequestProducer {

	public void alertRequestApproval(AlertApprovalDto alertApprovalDto) {

		Properties properties = new Properties();
		properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
		properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
		properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, AlertApproalDtoSerializer.class);
		properties.put(ProducerConfig.ACKS_CONFIG, "all");

		try (Producer<String, AlertApprovalDto> producer = new KafkaProducer<>(properties)) {
			ProducerRecord<String, AlertApprovalDto> producerRecord = new ProducerRecord<String, AlertApprovalDto>(
					GlobalConstants.KAFKA_ALERT_NEW_REQUEST_APPROVAL_TOPIC, GlobalConstants.KAFKA_ASSET_REQUEST_KEY,
					alertApprovalDto);
			producer.send(producerRecord, (metadata, exception) -> {
				if (Objects.isNull(exception)) {
					log.info("Request alert sent to topic:{} with key:{} and value:{}", metadata.topic(),
							GlobalConstants.KAFKA_ASSET_REQUEST_KEY, alertApprovalDto);
				} else {
					log.error(GlobalConstants.SERIALIZER_ERROR);
				}
			});
		}
	}

}
