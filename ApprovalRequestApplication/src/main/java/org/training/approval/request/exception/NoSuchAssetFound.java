package org.training.approval.request.exception;

import org.training.approval.request.constants.GlobalConstants;

public class NoSuchAssetFound extends GlobalException {

	private static final long serialVersionUID = 1L;

	public NoSuchAssetFound(String errorMessage) {
		super(GlobalConstants.ERROR_NOT_FOUND, GlobalConstants.NOT_FOUND, errorMessage);
	}

}
