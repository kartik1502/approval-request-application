package org.training.approval.request.kafka.configuration;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;
import org.training.approval.request.constants.GlobalConstants;

@Configuration
public class TopicConfigurations {

	@Bean
	NewTopic alertRequestTopic() {
		return TopicBuilder.name(GlobalConstants.KAFKA_ALERT_NEW_REQUEST_APPROVAL_TOPIC)
				.partitions(5).build();
	}
	
	@Bean
	NewTopic alertActionApprovalTopic() {
		return TopicBuilder.name(GlobalConstants.KAFKA_ALERT_ACTION_REQUEST_APPROVAL_TOPIC)
				.partitions(5).build();
	}
}
