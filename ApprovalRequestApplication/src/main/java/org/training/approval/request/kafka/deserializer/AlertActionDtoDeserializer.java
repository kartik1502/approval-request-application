package org.training.approval.request.kafka.deserializer;

import java.io.IOException;
import java.util.Objects;

import org.apache.kafka.common.errors.SerializationException;
import org.apache.kafka.common.serialization.Deserializer;
import org.training.approval.request.constants.GlobalConstants;
import org.training.approval.request.dto.AlertActionDto;

import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class AlertActionDtoDeserializer implements Deserializer<AlertActionDto> {

	@Override
	public AlertActionDto deserialize(String topic, byte[] data) {
		
		if(Objects.isNull(data)) {
			log.error(GlobalConstants.NULL_VALUE_DESERIALIZER);
			return null;
		}
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.readValue(data, AlertActionDto.class);
		} catch (IOException e) {
			throw new SerializationException(GlobalConstants.DESERIALIZER_ERROR);
		}
	}

	
}
