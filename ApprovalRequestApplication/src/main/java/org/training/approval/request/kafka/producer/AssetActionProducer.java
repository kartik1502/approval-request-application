package org.training.approval.request.kafka.producer;

import java.util.Objects;
import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.stereotype.Service;
import org.training.approval.request.constants.GlobalConstants;
import org.training.approval.request.dto.AlertActionDto;
import org.training.approval.request.kafka.serializer.AlertActionDtoSerializer;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AssetActionProducer {

	public void alertAssetAction(AlertActionDto alertActionDto) {

		Properties properties = new Properties();
		properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
		properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
		properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, AlertActionDtoSerializer.class);
		properties.put(ProducerConfig.ACKS_CONFIG, "all");

		try (Producer<String, AlertActionDto> producer = new KafkaProducer<>(properties)) {
			ProducerRecord<String, AlertActionDto> producerRecord = new ProducerRecord<String, AlertActionDto>(
					GlobalConstants.KAFKA_ALERT_ACTION_REQUEST_APPROVAL_TOPIC, GlobalConstants.KAFKA_ACTION_KEY,
					alertActionDto);
			producer.send(producerRecord, (metadata, exception) -> {
				if (Objects.isNull(exception)) {
					log.info("Message:{} sent to topic:{} with key:{}", alertActionDto, metadata.topic(),
							GlobalConstants.KAFKA_ACTION_KEY);
				} else {
					log.error(GlobalConstants.KAFKA_PRODUCER_ERROR);
				}
			});
		}

	}
}
