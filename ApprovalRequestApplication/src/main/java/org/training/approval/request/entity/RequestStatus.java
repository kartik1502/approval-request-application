package org.training.approval.request.entity;

public enum RequestStatus {
	SUBMITTED, RE_SUBMITTED, APPROVED, REJECTED, REFER_BACK, PENDING
}
