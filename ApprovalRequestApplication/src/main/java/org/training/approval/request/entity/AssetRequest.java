package org.training.approval.request.entity;

import java.time.LocalDateTime;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AssetRequest {

	@Id
	private String assetRequestId;
	
	@ManyToOne
	@JoinColumn(name = "assetId")
	private Asset asset;
	
	@ManyToOne
	@JoinColumn(name = "requesterId")
	private Employee requesterId;
	
	@ManyToOne
	@JoinColumn(name = "approverId")
	private Employee approverId;
	
	private String reason;
	
	@CreationTimestamp
	private LocalDateTime requestCreationTime;
	
	@UpdateTimestamp
	private LocalDateTime requestUpdateTime;
	
	@Enumerated(EnumType.STRING)
	private RequestStatus requestStatus;
}
