package org.training.approval.request.exception;

import java.util.List;

public record GlobalErrorResponse(List<String> errors, String errorCode) {

}
