package org.training.approval.request.exception;

import org.training.approval.request.constants.GlobalConstants;

public class NoSuchAssetRequest extends GlobalException {

	private static final long serialVersionUID = 1L;

	public NoSuchAssetRequest(String errorMessage) {
		super(GlobalConstants.ERROR_NOT_FOUND, GlobalConstants.NOT_FOUND, errorMessage);
	}

}
