package org.training.approval.request.dto;

import org.training.approval.request.constants.GlobalConstants;

import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;

public record ActionDto(String approverId,
		@Pattern(regexp = "(APPROVED)|(REJECTED)|(REFER_BACK)", message = GlobalConstants.REQUEST_STATUS_VALIDATION) String requestStatus,
		@Size(max = 250, message = GlobalConstants.RESTRICT_LENGTH) String reason) {

}
