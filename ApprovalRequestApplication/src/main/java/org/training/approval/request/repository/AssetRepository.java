package org.training.approval.request.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.training.approval.request.entity.Asset;

public interface AssetRepository extends JpaRepository<Asset, String> {

}
