package org.training.approval.request.kafka.consumer;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import org.training.approval.request.constants.GlobalConstants;
import org.training.approval.request.dto.AlertApprovalDto;
import org.training.approval.request.service.RequestApprovalService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
@RequiredArgsConstructor
public class AlertApprovalConsumer {

	private final RequestApprovalService requestApprovalService;
	
	@KafkaListener(topics = GlobalConstants.KAFKA_ALERT_NEW_REQUEST_APPROVAL_TOPIC, groupId = "alert-approval", containerFactory = "concurrentKafkaListenerContainerFactory")
	public void alertRequestApproval(ConsumerRecord<String, AlertApprovalDto> message) {
		
		log.info("Message: {} from topic:{} with key: {}", message.value(), message.topic(), message.key());
		requestApprovalService.updateRequestApproval(message.value());

	}

}
