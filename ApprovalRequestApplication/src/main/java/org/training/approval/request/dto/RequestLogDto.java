package org.training.approval.request.dto;

import java.time.LocalDateTime;

public record RequestLogDto(String requestLogId, String action, String actionBy, String remarks,
		LocalDateTime actionOn) {

}
