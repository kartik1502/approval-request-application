package org.training.approval.request.service;

import org.training.approval.request.dto.DashboardDto;

public interface DashboardService {

	DashboardDto viewDashboard(String employeeId);

}
