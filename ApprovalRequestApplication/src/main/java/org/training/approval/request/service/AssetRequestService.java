package org.training.approval.request.service;

import org.training.approval.request.dto.AssetRequestDto;

public interface AssetRequestService {

	AssetRequestDto viewRequestDetails(String assetRequestId, String employeeId);

}
