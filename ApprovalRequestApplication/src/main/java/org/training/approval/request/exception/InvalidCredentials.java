package org.training.approval.request.exception;

import org.training.approval.request.constants.GlobalConstants;

public class InvalidCredentials extends GlobalException {

	private static final long serialVersionUID = 1L;

	public InvalidCredentials(String errorMessage) {
		super(GlobalConstants.ERROR_UN_AUTHORIZED, GlobalConstants.UN_AUTHORIZED ,errorMessage);
	}

	
}
