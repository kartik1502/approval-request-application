package org.training.approval.request.dto;

public record ResponseDto(String responseCode, String responseMessage) {

}
