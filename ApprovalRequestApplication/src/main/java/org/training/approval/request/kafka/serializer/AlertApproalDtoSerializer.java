package org.training.approval.request.kafka.serializer;

import java.util.Objects;

import org.apache.kafka.common.errors.SerializationException;
import org.apache.kafka.common.serialization.Serializer;
import org.training.approval.request.constants.GlobalConstants;
import org.training.approval.request.dto.AlertApprovalDto;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class AlertApproalDtoSerializer implements Serializer<AlertApprovalDto> {

	@Override
	public byte[] serialize(String topic, AlertApprovalDto data) {

		if (Objects.isNull(data)) {
			log.error(GlobalConstants.NULL_VALUE_SERIALIZER);
			return null;
		}
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.writeValueAsBytes(data);
		} catch (JsonProcessingException e) {
			throw new SerializationException(GlobalConstants.SERIALIZER_ERROR);
		}
	}

}
