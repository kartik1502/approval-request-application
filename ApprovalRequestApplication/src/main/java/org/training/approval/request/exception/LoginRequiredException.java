package org.training.approval.request.exception;

import org.training.approval.request.constants.GlobalConstants;

public class LoginRequiredException extends GlobalException {

	private static final long serialVersionUID = 1L;

	public LoginRequiredException(String errorMessage) {
		super(GlobalConstants.ERROR_LOGIN_REQUIRED, GlobalConstants.UN_AUTHORIZED, errorMessage);
	}

}
