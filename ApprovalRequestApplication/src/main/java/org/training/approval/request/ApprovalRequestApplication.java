package org.training.approval.request;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApprovalRequestApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApprovalRequestApplication.class, args);
	}

}
