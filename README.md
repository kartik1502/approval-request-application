# Request Approval Application

The request approval application centralizes the management of the requests from multiple applications, providing a unified dashboard to view, manage and approve the requests. Key features include detailed request information, allowing the user to access comprehensive specifics about each request. This also enables the user to approve or reject requests with a single click and add comments or feedback if necessary.

## Services

- **Leave Management System**: Handles all the requested related to the leave including the employee requesting for leave, tracking the leave status by employee. Also, the manager can review the leave request and update the status of the leave request raised. 
- **Asset Management System**: Handles all the requests related to the assets including the employees raising the requests for different hardware assets like laptop, earphones and mouse etc., surrendering the assets received. Also, the manager can review the request raised and update its status.
- **Reimbursment System**: Handles all the requests including to avail the reimbursement for the monthly mobile charges, Broadband charges and other travel charges. Also allows the manager to review the request and update the status of the request.

## Database Design

### Employee

| Field Name | Data type | Null | Key | Default |
| ---------- | --------- | ---- | --- | ------- |
| employee_id |  VARCHAR(255) | NO | PRIMARY KEY | auto_increment |
| first_name | VARCHAR(255) | YES | | NULL |
| last_name | VARCHAR(255) | YES | | NULL |
| email_id | VARCHAR(255) | YES | | NULL |
| passowrd | VARCHAR(255) | YES | | NULL |
| contact_number | VARCHAR(255) | YES | | NULL |
| manager_id | VARCHAR(255) | YES | FOREIGN_KEY | NULL |
| login_status | ENUM('LOGGED_IN', 'LOGGED_OUT') | YES | | NULL |

### Leave Request

| Field Name | Data type | Null | Key | Default |
| ---------- | --------- | ---- | --- | ------- |
| leave_id | VARCHAR(255) | NO | PRIMARY_KEY | auto_increment |
| from_date | DATETIME | YES | | NULL |
| to_date | DATETIME | YES | | NULL |
| reason | VARCHAR(255) | YES | | NULL |
| requestor_id | VARCHAR(255) | YES | FOREIGN_KEY | NULL |
| approver_id | VARCHAR(255) | YES | FOREIGN_KEY | NULL |
| request_creation_time | DATETIME | YES | | NULL |
| request_update_time | DATETIME | YES | | NULL |
| request_status | ENUM('APPROVED','REJECT','REFER_BACK'.'SUBMITTED','RE_SUBMITTED')  | YES | | NULL |

### Asset Request

| Field Name | Data type | Null | Key | Default |
| ---------- | --------- | ---- | --- | ------- |
| asset_request_id | VARCHAR(255) | NO | PRIMARY_KEY | auto_increment |
| asset_id | VARCHAR(255) | YES | FOREIGN_KEY | NULL |
| requestor_id | VARCHAR(255) | YES | FOREIGN_KEY | NULL |
| approver_id | VARCHAR(255) | YES | FOREIGN_KEY | NULL |
| reason | VARCHAR(255) | YES | | NULL |
| request_creation_time | DATETIME | YES | | NULL |
| request_update_time | DATETIME | YES | | NULL |
| request_status | ENUM('APPROVED','REJECT','REFER_BACK'.'SUBMITTED','RE_SUBMITTED')  | YES | | NULL |

### Asset Table

| Field Name | Data type | Null | Key | Default |
| ---------- | --------- | ---- | --- | ------- |
| asset_id | VARCHAR(255) | NO | PRIMARY_KEY | auto_increment |
| asset_name | VARCHAR(255) | YES | FOREIGN_KEY | NULL |
| serial_number | VARCHAR(255) | YES | FOREIGN_KEY | NULL |
| asset_specification | VARCHAR(255) | YES | FOREIGN_KEY | NULL |
| description | VARCHAR(255) | YES | | NULL |
| quantity | DATETIME | YES | | NULL |

### Reimbursment Request 
| Field Name | Data type | Null | Key | Default |
| ---------- | --------- | ---- | --- | ------- |
| reimburment_id | VARCHAR(255) | NO | PRIMARY_KEY | auto_increment |
| reimburment_type | ENUM('MOBILE_BILL', 'BROADBAND_BILL', 'TRAVEL_EXPENSES', 'ACCOMADATION') | YES | | NULL |
| amount | DECIMAL() | YES | | NULL |
| alloated_amount | DECIMAL() | YES | | NULL |
| requestor_id | VARCHAR(255) | YES | FOREIGN_KEY | NULL |
| approver_id | VARCHAR(255) | YES | FOREIGN_KEY | NULL |
| request_creation_time | DATETIME | YES | | NULL |
| request_update_time | DATETIME | YES | | NULL |
| request_status | ENUM('APPROVED','REJECT','REFER_BACK'.'SUBMITTED','RE_SUBMITTED')  | YES | | NULL |

### Request Approval

| Field Name | Data type | Null | Key | Default |
| ---------- | --------- | ---- | --- | ------- |
| request_id | VARCHAR(255) | NO | PRIMARY_KEY | auto_increment |
| source_application_id | ENUM('LMS', 'ASSET', 'REIMB') | YES | | NULL |
| source_request_id | VARCHAR(255) | YES | | NULL |
| requestor_id | VARCHAR(255) | YES | FOREIGN_KEY | NULL |
| approver_id | VARCHAR(255) | YES | FOREIGN_KEY | NULL |
| creation_time | DATETIME | YES | | NULL |
| update_time | DATETIME | YES | | NULL |
| request_status | ENUM('APPROVED', 'REJECT', 'REFER_BACK') | YES | | NULL |

### Request Log Table

| Field Name | Data type | Null | Key | Default |
| ---------- | --------- | ---- | --- | ------- |
| request_log_id | VARCHAR(255) | NO | PRIMARY_KEY | auto_increment |
| request_id | VARCHAR(255) | YES | FOREIGN_KEY | NULL |
| action | VARCHAR(255) | YES | FOREIGN_KEY | NULL |
| action_by | VARCHAR(255) | YES | FOREIGN_KEY | NULL |
| action_on | VARCHAR(255) | YES | | NULL |

## Use Cases

Below depicts complete workflow how the entire process occurs:

### Use Case: Login Functionality

- An employee logs into the application with the credentials,
    - If the **credentials are correct**, the employee is directed to the dashboard of the application.
    - If the **credentials are invalid**, a valid message is provided to the employee.

### Use Case: View Dashboard

- When the employee logs into application he would redirected to the dashboard, where a user view all the requests raised by the user.
- User can also view all the requests that are waiting for his action.

#### My Requests(requests raised by Employee)
| Request Id | Source Application | Source Request Id | Approver Name | Request Status |
| ---------- | ------------------ | ----------------- | ------------- | -------------- |

#### My Approvals(requests waiting for Employee Approval)
| Request Id | Source Application | Source Request Id | Approver Name | Request Status | Action |
| ---------- | ------------------ | ----------------- | ------------- | -------------- | ------ |
| | | | | | **APPROVE REJECT REFER_BACK** |

### Use Case: Raise Request

- Employee raises request for a service, a request is raised with all the details specified for a particular request.
- The detials of the request raised is stored in the database dedicated for the service.
- A message is also sent it the Request Approval application, with the detials as given below:

```json
{
    "sourceRequestId" : "String", 
    "requestDetails" : "String",
    "requestStatus" : "String",
    "approverId" : "String"
}
```

### User Case: Take Action Functionality

- An Approver(employee) takes action on the requests, that are waiting for his approval.
- He can either APPROVE, REFER BACK or REJECT the request that, when an is taken by the Employee the information is updated in the database.
- A message is sent to the source application from which the request was raised.

```json
{
    "resquestId" : "String",
    "approverId" : "String",
    "requestStatus" : "String",
    "approverId" : "String"
}
```

